# README #

brew install gphoto2
brew install yarn

# dev mode

yarn setup
yarn start

# build

yarn setup
yarn build
yarn serve

# run in electron

yarn setup
yarn build
yarn electron

# browser url
http://localhost:8080/app

# issues installing with new node

remove package-lock.json from src and app

# commands

take a picture : ENTER
print current picture: SPACEBAR
