import React from "react";

import './VideoBackground.css';

const VideoBackground = ({ src }) => {
    return <div className="VideoBackground-container" >
        <video className="VideoBackground-video" loop autoPlay>
            <source src={src} type="video/mp4" />
        </video>
    </div>;
}

export { VideoBackground }