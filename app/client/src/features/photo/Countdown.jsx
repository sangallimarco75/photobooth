import React, { Component } from "react";

import './Countdown.css';

class Countdown extends Component {
    render() {
        const { counter } = this.props;
        return (<div className="countdown">{counter}</div>);
    }
}

export { Countdown };