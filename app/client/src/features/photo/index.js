import { PhotoStore } from "./PhotoStore";

export const photoStore = new PhotoStore();

export { Photo } from "./Photo";
