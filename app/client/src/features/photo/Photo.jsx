import React, { Component } from "react";
import { Loading } from "./Loading";
import { observer } from "mobx-react";
import { Countdown } from './Countdown';
import * as _ from 'lodash';

import './Photo.css';

@observer
class Photo extends Component {
  renderImg(src) {
    return src ? <img className="photo" src={src} alt="" /> : null;
  }

  render() {
    const { photoStore } = this.props;
    const { imgSrc, loading, message } = photoStore;
    const src = !_.isEmpty(imgSrc) ? `/${imgSrc}` : null;

    return (
      <div>
        <Countdown counter={message} />
        <Loading visible={!loading}>
          {this.renderImg(src)}
        </Loading>
      </div>
    );
  }
}

export { Photo };
