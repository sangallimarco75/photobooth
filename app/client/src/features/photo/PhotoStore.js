import {
  action,
  observable
} from "mobx";
import {
  webSocketService
} from "../../core/websocket";

const PROCESSING_MESSAGE = 'Attendere... Čakajte...';
const PRINTING_MESSAGE = 'Stampando... Tlač...';

class PhotoStore {
  @observable imgSrc = null;
  @observable loading = false;
  @observable message = '';

  timer = null;
  lock = false;

  constructor() {
    webSocketService.on("/photo", data => {
      const {
        imgSrc
      } = data;
      this.setLoading(false);
      this.setPhoto(imgSrc);
    });
    webSocketService.on("/print", data => {
      this.setLoading(true, PRINTING_MESSAGE);
      setTimeout(() => {
        this.setLoading(false);
      }, 2000);
    });
  }

  @action
  setPhoto(imgSrc) {
    this.imgSrc = imgSrc;
  }

  @action
  setLoading(status, message) {
    this.loading = status;
    this.message = message || '';
  }

  @action
  takePicture() {
    if (this.loading) {
      return;
    }
    webSocketService.send("/photo", {
      status: true
    });
    this.loading = true;
  }

  @action
  printPicture() {
    if (this.loading) {
      return;
    }
    webSocketService.send("/print", {
      imgSrc: this.imgSrc
    });
    this.loading = true;
  }

  @action
  test() {
    webSocketService.send("/test", {
      status: true
    });
    this.setLoading(false, PROCESSING_MESSAGE);
    this.loading = true;
  }

  @action
  countDown(counter, reset = false) {

    if (reset) {
      this.imgSrc = null;
      this.loading = false;
      if (this.timer) {
        clearTimeout(this.timer);
      }
    }

    this.setLoading(false, counter.toString());

    if (counter > 0) {
      this.timer = setTimeout(() => {
        this.countDown(counter - 1)
      }, 1000);
    } else {
      this.setLoading(false, PROCESSING_MESSAGE);
      this.takePicture();
    }
  }
}

export {
  PhotoStore
};