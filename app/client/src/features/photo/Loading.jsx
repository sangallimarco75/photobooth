import React, { Component } from "react";
import "./Loading.css";

class Loading extends Component {
  render() {
    const { visible, children } = this.props;
    const className = visible ? "loading" : "loading loading-hidden";
    return <div className={className}>{children}</div>;
  }
}

export { Loading };
