import ReconnectingWebSocket from 'reconnecting-websocket';

class WebSocketService {
  constructor(uri) {
    const { location: { port } } = window;
    uri = uri || `ws://localhost:${port}/ws`;

    this.routes = [];
    this.ws = new ReconnectingWebSocket(uri);
    // this.ws.addEventListener('connect', this.onMessage);
    this.ws.addEventListener("message", this.onMessage);
  }

  onMessage = event => {
    // console.log(event);

    const { data: payload } = event;
    try {
      const payloadObject = JSON.parse(payload);
      const { uri, data } = payloadObject;
      const route = this.routes.find(x => x.uri === uri);
      if (route) {
        route.callback(data);
      }
    } catch (e) { }
  };

  on(uri, callback) {
    const route = { uri, callback };
    this.routes.push(route);
  }

  destroy(listener) {
    //
  }

  send(uri, data = {}) {
    const msg = JSON.stringify({ uri, data });
    this.ws.send(msg);
  }
}


export const webSocketService = new WebSocketService();
