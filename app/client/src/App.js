import React, { Component } from "react";
import "./App.css";
import { Provider } from "mobx-react";

import keydown from "react-keydown";
import { Photo, photoStore } from "./features/photo";
import { VideoBackground } from "./features/VideoBackGround";

const stores = { photoStore };

class App extends Component {

  @keydown("enter")
  submit(event) {
    event.preventDefault();
    // photoStore.takePicture();
    photoStore.countDown(10, true);
  }

  @keydown("tab")
  print(event) {
    event.preventDefault();
    photoStore.printPicture();
  }

  @keydown("t")
  test(event) {
    event.preventDefault();
    photoStore.test();
  }

  render() {
    return (
      <Provider {...stores}>
        <div className="App">
          <Photo photoStore={photoStore} />
          <VideoBackground src="/assets/videoplayback.mp4" />
        </div>
      </Provider>
    );
  }
}

export default App;
