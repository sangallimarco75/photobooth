const sharp = require("sharp");
const path = require("path");

function postProcessing(imgSrc, imgOverlay) {
  const dir = path.dirname(imgSrc);
  const fileName = path.basename(imgSrc);
  const imgDest = `${dir}/_${fileName}`;
  const padding = 50;
  
  return new Promise((resolve, reject) => {
    sharp(imgSrc)
      // .normalise()
      // .convolve({
      //   width: 3,
      //   height: 3,
      //   kernel: [
      //     -1, -1, -1,
      //     -1, 9, -1,
      //     -1, -1, -1
      //   ]
      // })
      // .background({ r: 255, g: 255, b: 255, alpha: 255 })
      // .extend({ top: padding, bottom: padding, left: padding, right: padding })
      .overlayWith(imgOverlay)
      // .withMetadata()
      .toFile(imgDest)
      .then(info => {
        resolve(imgDest);
      })
      .catch(err => {
        reject(err);
      });
  });
}

module.exports = postProcessing;
