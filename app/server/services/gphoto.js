/**
 * brew install gphoto2
 * brew install imagemagick
 * https://www.cups.org/doc/options.html
 */

const { exec } = require("child_process");

function takePicture(dest) {
  return new Promise((resolve, reject) => {
    exec(
      `gphoto2 --capture-image-and-download --filename=${dest}`,
      (err, stdout, stderr) => {
        if (err) {
          reject(err);
        } else {
          resolve(dest);
        }
      }
    );
  });
}

module.exports = takePicture;
