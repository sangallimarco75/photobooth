const path = require('path');

class PathUtils {
    constructor(appPath) {
        this.appPath = `${appPath}/`;
    }

    absoluteToRelative(filePath) {
        return filePath.split(this.appPath)[1];
    }

    relativeToAbsolute(filePath) {
        return path.join(this.appPath, filePath);
    }

}

module.exports = PathUtils;
