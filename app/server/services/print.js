const {
  exec
} = require("child_process");

function print(src) {
  return new Promise((resolve, reject) => {
    exec(
      `lp -o landscape ${src}`,
      (err, stdout, stderr) => {
        if (err) {
          console.log('error');
          reject(err);
        } else {
          console.log('ok');
          resolve();
        }
      }
    );
  });
}

module.exports = print;