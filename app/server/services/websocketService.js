const base64Img = require("base64-img");

class WebSocketService {
  buildMessage(uri, data) {
    return JSON.stringify({ uri, data });
  }
}

module.exports = new WebSocketService();
