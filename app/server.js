const express = require('express');
const expressWs = require('express-ws');
const path = require('path');
const os = require('os');

const {
  webSocketRouter,
  webSocketMiddleware,
  webSocketService
} = require('./server/services');
const {
  app
} = expressWs(express());

const takePicture = require('./server/services/gphoto');
const postProcessing = require('./server/services/postprocessing');
const printPicture = require('./server/services/print');
const PathUtils = require('./server/services/pathUtils');

const PICTURES_PATH = `${os.homedir()}/Pictures`;
const rootPath = new PathUtils(__dirname);
const picturePath = new PathUtils(PICTURES_PATH);

// routes
webSocketRouter.on('/photo', (ws, uri, data) => {
  const ts = new Date().getTime();
  const filename = picturePath.relativeToAbsolute(`${ts}.jpg`);
  const overlayFilename = rootPath.relativeToAbsolute('assets/frame.png');

  // take a picture
  takePicture(filename)
    .then(src => {
      postProcessing(filename, overlayFilename)
        .then(src => {
          const imgSrc = picturePath.absoluteToRelative(src);
          const message = webSocketService.buildMessage('/photo', {
            imgSrc
          });
          ws.send(message);
        })
        .catch(err => {
          const {
            message
          } = err;
          console.error(message);
        });
    })
    .catch(err => {
      const {
        message
      } = err;
      console.error(message);
    });
});

// print
webSocketRouter.on('/print', (ws, uri, data) => {
  const {
    imgSrc
  } = data;
  const filePath = picturePath.relativeToAbsolute(imgSrc);
  printPicture(filePath)
    .then(res => {
      const message = webSocketService.buildMessage('/print', {
        imgSrc
      });
      ws.send(message);
    }).catch(err => {
      const {
        message
      } = err;
      console.error(message);
    });
});

// testing post processing
webSocketRouter.on('/test', (ws, uri, data) => {
  const filename = rootPath.relativeToAbsolute('assets/test.jpg');
  const overlayFilename = rootPath.relativeToAbsolute('assets/frame.png');

  // test postprocessing
  postProcessing(filename, overlayFilename)
    .then(src => {
      const imgSrc = 'assets/_test.jpg';
      const message = webSocketService.buildMessage('/photo', {
        imgSrc
      });
      ws.send(message);
    })
    .catch(err => {
      const {
        message
      } = err;
      console.error(message);
    });
});

app.use('/ws', webSocketMiddleware);
app.use('/app', express.static(rootPath.relativeToAbsolute('client/build')));
app.use('/', express.static(PICTURES_PATH));
app.use('/assets', express.static(rootPath.relativeToAbsolute('assets')));

app.listen(8080);